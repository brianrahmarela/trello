import React, { useState } from "react";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
// import img1 from '../assets/images/img_dashboard.jpg';
import { Card } from "antd";
const { Meta } = Card;

const finalSpaceCharacters = [
  {
    id: "gary",
    name: "Gary Goodspeed",
    // thumb: "../assets/images/img_dashboard.jpg",
  },
  {
    id: "cato",
    name: "Little cato",
    // thumb: "../assets/images/img_dashboard.jpg",
  },
  {
    id: "kvn",
    name: "KVN",
    // thumb: "../assets/images/img_dashboard.jpg",
  },
  {
    id: "mooncake",
    name: "Moon cake",
    // thumb: "../assets/images/img_dashboard.jpg",
  },
];

function Cards() {
  const [characters, updateCharacters] = useState(finalSpaceCharacters);

  function handleOnDragEnd(result) {
    if (!result.destination) return;

    const items = Array.from(characters);
    const [reorderedItem] = items.splice(result.source.index, 1);
    items.splice(result.destination.index, 0, reorderedItem);

    updateCharacters(items);
  }
  return (
    <>
    <div className="site-card-border-less-wrapper">
      <Card bordered={false} style={{ width: 300}} className="ListStyle">
      <Meta title="List 1" style={{ margin: "0px 0px 10px 7px", marginTop: "10px"}}/>

        <DragDropContext onDragEnd={handleOnDragEnd}>
          <Droppable droppableId="characters">
            {(provided) => (
              <ul
                className="characters"
                {...provided.droppableProps}
                ref={provided.innerRef}
              >
                {characters.map(({ id, name }, index) => {
                  return (
                    <Draggable key={id} draggableId={id} index={index}>
                      {(provided) => (
                        <li
                          ref={provided.innerRef}
                          {...provided.draggableProps}
                          {...provided.dragHandleProps}
                        >
                          {/* <div className="characters-thumb">
                        <img src={thumb} alt={`${name} Thumb`} />
                      </div> */}
                          <p style={{color: "#212121"}}>{name}</p>
                        </li>
                      )}
                    </Draggable>
                  );
                })}
                {provided.placeholder}
              </ul>
            )}
          </Droppable>
        </DragDropContext>
        {/* <p>Card content</p> */}
      </Card>
    </div>
    
  </>
  );
}

export default Cards;
