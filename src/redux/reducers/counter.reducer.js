import { INCREMENT, DECREMENT } from "../actions/counter.actions";

const initialState = {
    counter : 1
}

const Counter = (state = initialState, action) => {
    switch(action.type){
        case INCREMENT:
            return{
                ...state,
                counter: state.counter +1,
            }
            case DECREMENT:
            return{
                ...state,
                counter: state.counter -1,
            }
        
        default:
            return state
    }
}

export default Counter;