// import {getData} from '../actions/counter.actions';
import {GET_DATA_REQUEST, GET_DATA_SUCCESS, GET_DATA_FAILED} from '../actions/user.actions';

const initialState = {
    data: [],
    error: null,
}

const getData = (state=initialState, action) => {
    switch(action.type){
        case GET_DATA_REQUEST:
            return{
                ...state,
            }
            case GET_DATA_SUCCESS:
            return{
                ...state,
                data: action.result,
            }
            case GET_DATA_FAILED:
                console.log(action.error);
            return{
                ...state,
                error: action.error
            }
        
        default:
            return state
    }
}

export default getData;