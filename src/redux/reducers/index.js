import { combineReducers } from "redux";

import Counter from './counter.reducer'
// import getData from './user.reducer'
import getBoard from './board.reducers'
import auth from './auth.reducers'

const rootReducers = combineReducers({Counter, auth, getBoard});

export default rootReducers;