// import logo from './logo.svg';
import "./App.css";
import { useSelector } from 'react-redux';

// pages
import Home from "./page/Home";
import Login from "./page/Login";
import SignUp from "./page/Signup";
import AppHeader from "./components/Header";
import Counter from "./components/Counter";
import Card from "./page/Card";
import List from "./page/List";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "antd/dist/antd.css";

import { Layout } from "antd";

const { Header } = Layout;

function App() {

  const isLogin = useSelector(state => state);
  console.log(isLogin)

  return (
    <Router>
      <Layout className="layout">
        <Header>
          <AppHeader />
        </Header>
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/signup">
            <SignUp />
          </Route>
          <Route path="/list">
            <List />
          </Route>
          <Route path="/card">
            <Card />
          </Route>
          <Route path="/counter">
            <Counter />
          </Route>
        </Switch>
        {/* <Content style={{ padding: '0 50px' }}>
      <Breadcrumb style={{ margin: '16px 0' }}>
        <Breadcrumb.Item>Home</Breadcrumb.Item>
        <Breadcrumb.Item>List</Breadcrumb.Item>
        <Breadcrumb.Item>App</Breadcrumb.Item>
      </Breadcrumb>
      <div className="site-layout-content">Content</div>
    </Content>
    <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer> */}
      </Layout>
      ,
    </Router>
  );
}

export default App;
