import axios from 'axios';

export const GET_BOARD_ID_REQUEST = "GET_BOARD_ID_REQUEST"
export const GET_BOARD_ID_SUCCESS= "GET_BOARD_ID_SUCCESS"
export const GET_BOARD_ID_FAILED = "GET_BOARD_ID_FAILED"

export const getBoardID_Request = () => {
  return{
    type: GET_BOARD_ID_REQUEST,
  }
}
export const getBoardID_Success = (result) => {
  return{
    type: GET_BOARD_ID_SUCCESS,
    result
  }
}
export const getBoardID_Error = (error) => {
  return{
    type: GET_BOARD_ID_FAILED,
    error,
  }
}

export const getBoardID = (item) => {
  return function (dispatch){
    dispatch(getBoardID_Request())
    axios
      .get(`http://localhost:8080/board/project/${item.id}`)
      .then(result => dispatch(getBoardID_Success(result.data)))
      .catch(error => dispatch(getBoardID_Error(error)))
  }
}