import {
  GET_BOARD_REQUEST, 
  GET_BOARD_SUCCESS,
  GET_BOARD_FAILED
} from '../actions/boardGet.actions';

import {
  POST_BOARD_REQUEST, 
  POST_BOARD_SUCCESS,
  POST_BOARD_FAILED
} from '../actions/boardPost.action';

import {
  GET_BOARD_ID_REQUEST, 
  GET_BOARD_ID_SUCCESS,
  GET_BOARD_ID_FAILED
} from '../actions/boardGetId.actions';


//   import {
//     DELETE_BOARD__REQUEST, 
//     DELETE_BOARD__SUCCESS,
//     DELETE_BOARD__FAILED
//   } from "../actions/deletetodo.action";

//   import {
//     PUT_BOARD__REQUEST, 
//     PUT_BOARD__SUCCESS,
//     PUT_BOARD__FAILED
//   } from "../actions/puttodo.action";

const initialState = {
  data:[],
  error: null,
  // isLoading: false,
};

const getBoard = (state = initialState, action) => {
  switch(action.type){
    //GET CASE
    case GET_BOARD_REQUEST:
      console.log('sudah request data', action);
      return{
        ...state,
        //   isLoading: true,
      }
    case GET_BOARD_SUCCESS:
      console.log('list board sukses');
      return{
        ...state,
      //   isLoading: false,
        data: action.result,
      }
    case GET_BOARD_FAILED:
      console.log('list board failed');
      return{
        ...state,
      //   isLoading: false,
        data: action.error,
      }
      
      // GET ID CASE
      case GET_BOARD_ID_REQUEST:
        console.log('sudah request get ID board', action);
        return{
          ...state,
          //   isLoading: true,
        }
      case GET_BOARD_ID_SUCCESS:
        console.log('list board sukses get ID board');
        return{
          ...state,
        //   isLoading: false,
          data: action.result,
        }
      case GET_BOARD_ID_FAILED:
        console.log('list board failed get ID board');
        return{
          ...state,
        //   isLoading: false,
          data: action.error,
        }
    
    // POST CASE
    case POST_BOARD_REQUEST:
      console.log('sudah post request data', action);
      // alert(state);

      return {

          ...state,

          // isLoading: true,
      };
  
    case POST_BOARD_SUCCESS:
        return {
            ...state,
            data: [
                ...state.data,
                action.result
            ],
          //   isLoading: false,
        };

    case POST_BOARD_FAILED:
        console.log(action.error);
        return {
            ...state,
            error: action.error,
          //   isLoading: false,
        };
    
  //   //DELETE CASE
  //   case DELETE_BOARD_REQUEST:
  //     return {
  //         ...state,
  //         // isLoading: true,
  //     };
  
  //   case DELETE_BOARD_SUCCESS:
  //       let newTodoList = state.data.filter(item => item.id !== action.result.id);
  //       console.log(newTodoList);
  //       return {
  //           ...state,
  //           data: [...newTodoList],
  //         //   isLoading: false,
  //       };

  //   case DELETE_BOARD_FAILED:
  //       console.log(action.error);
  //       return {
  //           ...state,
  //           error: action.error,
  //         //   isLoading: false,
  //       };

  //   //UPDATE CASE
  //   case PUT_BOARD_REQUEST:
  //           return {
  //               ...state,
  //             //   isLoading: true,
  //           };
        
  //   case PUT_BOARD_SUCCESS:
  //       let updatedTodoList = state.data.map(item => {
  //           if(item.id === action.result.id){
  //               item.todo = action.result.todo
  //           }
  //           return item;
  //       })

  //       return {
  //           ...state,
  //           data: [...updatedTodoList],
  //         //   isLoading: false,
  //       };

  //   case PUT_BOARD_FAILED:
  //       console.log(action.error);
  //       return {
  //           ...state,
  //           error: action.error,
  //         //   isLoading: false,
  //       };

    default:
      return state;
  }
}

export default getBoard;