import { useDispatch, useSelector } from "react-redux";
import React, { useState, useEffect } from "react";
// import { Card } from "antd";
import { Layout } from "antd";
import { Row, Col } from "antd";
import { Image } from "antd";
import img1 from "../assets/images/img_dashboard.jpg";
import { Button, Modal, Form, Input } from "antd";
import { useHistory } from "react-router-dom";
import {getBoard} from "../redux/actions/boardGet.actions";
// import {getBoardID} from "../redux/actions/boardGetId.actions";
import { postBoard } from "../redux/actions/boardPost.action";

const { Content } = Layout;
const { Item } = Form;

function Dashboard() {
  const history = useHistory();
  const ListBoard = useSelector((state) => state.getBoard.data);
  console.log('get api',ListBoard);
  const dispatch = useDispatch();
  
  const [modal, setModal] = useState(false);
  const [board, setBoard] = useState({
    name: "",
    description: "",
  });
  
  useEffect(() => {
   dispatch((getBoard()));
 }, [dispatch])

  const addNewBoard = () => {
    // e.preventDefault();

    setBoard({
      ...board,
      name: "",
      description: "",
    });
    setModal(true);
  };

  const closeNewBoard = () => {
    // history.push("/card")

    setModal(false);
  };

  const action = () => {
    console.log("data new board sblm di dispatch", board);
    dispatch(postBoard(board));
    closeNewBoard();
    // history.push('/list');
  };
  console.log("state board after create new board", board);

  // const layout = {
  //   labelCol: {
  //     span: 8,
  //   },
  //   wrapperCol: {
  //     span: 16,
  //   },
  // };

  const handleClickBoard = (e, item) => {
    // console.log('list dari API', ListBoard);
    history.push('/list');
    console.log('e board id', e)
    console.log('item board id', item.id)
    
  };

  const handleChange = (e) => {
    console.log("handle change", e);
    const { name, value } = e.target;
    setBoard({
      ...board,
      [name]: value,
    });
    console.log(board);
  };

  return (
    <div>
      <Layout className="layout">
        <Content style={{ padding: "0 50px" }}>
          <Row gutter={[16, 16]}>

              {ListBoard === undefined
                ? 
                history.push("/")
                  // console.log("undefined")
                : ListBoard.map(
                  (item, index) => (
                    // console.log('item board: ', item),
                      <Col span={8} xs={24} md={12} lg={8} key={index}>
                        <div className="image">
                          <Image className="image__img" src={img1} />
                          <div className="image__overlay image__overlay--primary"onClick={(e) => handleClickBoard(e, item)}>
                            <div className="image__title">{item.name}</div>
                            <p className="image__description">{item.description}</p>
                          </div>
                        </div>
            </Col>
                    )
                  )}
                 
           

            {/* <Col span={8} xs={24} md={12} lg={8}>
              <div className="image">
                <Image className="image__img" src={img1} />
                <div className="image__overlay image__overlay--primary">
                  <div className="image__title">Board 2</div>
                  <p className="image__description">Description</p>
                </div>
              </div>
            </Col>
            <Col span={8} xs={24} md={12} lg={8}>
              <div className="image">
                <Image className="image__img" src={img1} />
                <div className="image__overlay image__overlay--primary">
                  <div className="image__title">Board 3</div>
                  <p className="image__description">Description</p>
                </div>
              </div>
            </Col>

            <Col span={8} xs={24} md={12} lg={8}>
              {" "}
              <div className="image">
                <Image className="image__img" src={img1} />
                <div className="image__overlay image__overlay--primary">
                  <div className="image__title">Board 4</div>
                  <p className="image__description">Description</p>
                </div>
              </div>
            </Col>
            <Col span={8} xs={24} md={12} lg={8}>
              {" "}
              <div className="image">
                <Image className="image__img" src={img1} />
                <div className="image__overlay image__overlay--primary">
                  <div className="image__title">Board 5</div>
                  <p className="image__description">Description</p>
                </div>
              </div>
            </Col> */}
            <Col span={8} xs={24} md={12} lg={8}>
              <Button
                type="primary"
                className="buttonAddNewBoard"
                onClick={addNewBoard}
              >
                Add New Board
              </Button>

              <Modal
                title="New Board"
                visible={modal}
                onCancel={closeNewBoard}
                onOk={action}
                footer={
                  // <Button onClick={closeNewBoard} key="cancelboard" name="cancelboard">Cancel</Button>,
                  <Button
                    onClick={action}
                    type="primary"
                    // key="createboard"
                    name="createboard"
                  >
                    Create Board
                  </Button>
                }
              >
                <Form>
                  <Item label="Name Board">
                    <Input
                      name="name"
                      onChange={handleChange}
                      value={board.name}
                    />
                  </Item>
                  <Item label="Detail Board" onChange={handleChange}>
                    <Input name="description" value={board.description} />
                  </Item>
                </Form>
              </Modal>
            </Col>
          </Row>
          {/* <Row gutter={[16, 16]}>
            <Col span={8}>Card7</Col>
            <Col span={8}>Card8</Col>
            <Col span={8}>Card9</Col>
          </Row> */}
        </Content>
        {/* <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer> */}
      </Layout>
    </div>
  );
}

export default Dashboard;
