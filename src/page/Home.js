import React from 'react'
import Dashboard from '../components/Dashboard'
import { Typography } from 'antd';

const { Title } = Typography;

function Home() {
  return (
    <div className="container-fluid">
      <div className="title" style={{ textAlign: 'center' }}>

      <Title level={2} className="titles">Your Workspace</Title>
      </div>
      <div className="contentHolder" >

      <Dashboard/>
      </div>
    </div>
  )
}

export default Home