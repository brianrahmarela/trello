import axios from 'axios';
import {getBoard} from '../actions/boardGet.actions';

export const POST_BOARD_REQUEST = "POST_BOARD_REQUEST"
export const POST_BOARD_SUCCESS= "POST_BOARD_SUCCESS"
export const POST_BOARD_FAILED = "POST_BOARD_FAILED"

export const postBoardRequest = () => {
  return{
    type: POST_BOARD_REQUEST,
  }
}
export const postBoardSuccess = (result) => {
  return{
    type: POST_BOARD_SUCCESS,
    result,
  }
}
export const postBoardError = (error) => {
  return{
    type: POST_BOARD_FAILED,
    error,
  }
}

export const postBoard = (newBoard) =>{
  return function (dispatch){
    console.log('ini data new board di action', newBoard);
    console.log('msk ke auth sblm axios');
    dispatch(postBoardRequest())
    axios
      .post('http://localhost:8080/board/posting',newBoard)
      .then(result => dispatch(postBoardSuccess(result)))
      .then(hasil => dispatch(getBoard(hasil)))
    //   .then(result => dispatch(postBoardSuccess(result.data)))
      .catch(error => dispatch(postBoardError(error)))
  }
  
}


