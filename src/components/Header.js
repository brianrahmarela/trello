import React from "react";
import { Menu } from "antd";
import { Link, useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";

import { logoutAction } from "../redux/actions/auth.actions";

function AppHeader() {
	const isLogin = useSelector((state) => state.auth);
	console.log(isLogin);
	const history = useHistory();
	const dispatch = useDispatch();

	const handleLogOut = () => {
		localStorage.clear();
		dispatch(logoutAction());
		history.push("/");
	};

	return (
		<div className="container-fluid">
			<div className="header">
				<div className="logo" />
				<Menu theme="dark" mode="horizontal" defaultSelectedKeys={["/"]}>
					<Menu.Item key="home">
						<Link to="/">Board</Link>
					</Menu.Item>
					{isLogin.isLogged ? ( 

					<Menu.Item key="logout" onClick={handleLogOut}>
						<Link to="login">Logout</Link>
					</Menu.Item>
					) : (

					<Menu.Item key="login">
						<Link to="login">Login</Link>
					</Menu.Item>
					)}

						
					<Menu.Item key="Signup">
						<Link to="Signup">Sign Up</Link>
					</Menu.Item>
			
					<Menu.Item key="list">
						<Link to="list">List </Link>
					</Menu.Item>
					<Menu.Item key="counter">
						<Link to="counter">Counter </Link>
					</Menu.Item>
				</Menu>
			</div>
		</div>
	);
}

export default AppHeader;
