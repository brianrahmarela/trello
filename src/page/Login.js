import React, { useState } from "react";
import { useHistory } from 'react-router-dom'
import { useDispatch } from 'react-redux';
import { Form, Input, Button, Checkbox } from "antd";
import { Typography } from 'antd';
import { loginAction } from "../redux/actions/auth.actions";

const { Title } = Typography;

function Login() {
  const history = useHistory();
  const dispatch = useDispatch();
  // const registerLoading = useSelector(state => state.auth)

  const [login, setLogin] = useState({
    name: "",
    password: ""
  })

  const [fail, setFail] = useState({
    result: true
  })

  const handleChange = (e) => {
    // console.log(e.target.value)
    setLogin({
      ...login,
      [e.target.name]: e.target.value
    })
  }

  const onFinish = (values) => {
    dispatch(loginAction(values, history, setLogin, setFail))
    console.log("Success:", values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div >
      <div className="title" style={{ textAlign: "center" }}>
        <Title level={2} className="titles">
          Login
        </Title>
      </div>{" "}
      <Form
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 8,
        }}
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item

          label="Username"
          name="username"
          // eslint-disable-next-line
          value={login.username} onChange={handleChange} name="username"
          rules={[
            {
              required: true,
              message: "Please input your username!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          // eslint-disable-next-line
          value={login.password} onChange={handleChange} name="password"
          rules={[
            {
              required: true,
              message: "Please input your password!",
            },
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          name="remember"
          valuePropName="checked"
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Checkbox>Remember me</Checkbox>
        </Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}

export default Login;
