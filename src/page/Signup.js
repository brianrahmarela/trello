import React, { useState, useEffect } from "react";
import { Form, Input, Button } from "antd";
import { Typography } from "antd";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router";
import { registerAction } from "../redux/actions/auth.actions";
import { Select } from "antd";

const { Title } = Typography;
const { Option } = Select;

const { Item } = Form;

function SignUp() {
  const history = useHistory();
  const dispatch = useDispatch();

  const [register, setRegister] = useState({
    name: "",
    username: "",
    email: "",
    role: [],
    password: "",
  });

  const handleChange = (e) => {
    // console.log(e);
    console.log(e.target.name);
    console.log(e.target.value);
    setRegister({
      ...register,
      [e.target.name]: e.target.value,
    });
  };
  console.log("ini data state register", register);

  const handleSelect = (e) => {
    console.log(e);
    // console.log(e.target);

    setRegister({
      ...register,
      role: [e],
    });
  };
  // console.log(handleSelect);

  console.log("ini data state register", register);

  const handleSubmit = (e) => {
    const { name, value } = e.target;

    setRegister({
      ...register,
      [name]: value,
    });
    dispatch(registerAction(register, history));

    console.log("data form submit signup", register);
  };

  useEffect(() => {}, [register]);

  return (
    <div>
      <div className="title" style={{ textAlign: "center" }}>
        <Title level={2} className="titles">
          SignUp
        </Title>
      </div>{" "}
      <Form
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 8,
        }}
      >
        <Item label="Name">
          <Input
            name="name"
            onChange={handleChange}
            // value={register.nameBoard}
          />
        </Item>
        <Item
          label="Username"
          onChange={handleChange}
          rules={[
            {
              required: true,
            },
          ]}
        >
          <Input name="username" />
        </Item>
        <Item label="Email" onChange={handleChange}>
          <Input name="email" />
        </Item>
        <Item
          name="role"
          label="Role"
          rules={[
            {
              required: true,
            },
          ]}
          // value={register.role}
        >
          <Select
            placeholder="Select a option and change input text above"
            onChange={handleSelect}
            value={register.role}
            allowClear
          >
            <Option value="user">User</Option>
            <Option value="moderator">Moderator</Option>
            <Option value="admin">Admin</Option>
            <Option value="superadmin">Super admin</Option>
          </Select>
        </Item>
        <Item
          label="password"
          onChange={handleChange}

          // rules={[
          //   {
          //     required: true,
          //   },
          // ]}
        >
          <Input
            name="password"
            // value={register.password}
            type="password"
            />
        </Item>
        <Item
          onChange={handleChange}
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button type="primary" htmlType="submit" onClick={handleSubmit}>
            Submit
          </Button>
        </Item>
      </Form>
    </div>
  );
}

export default SignUp;
