import axios from 'axios';

export const GET_BOARD_REQUEST = "GET_BOARD_REQUEST"
export const GET_BOARD_SUCCESS= "GET_BOARD_SUCCESS"
export const GET_BOARD_FAILED = "GET_BOARD_FAILED"

export const getBoardRequest = () => {
  return{
    type: GET_BOARD_REQUEST,
  }
}
export const getBoardSuccess = (result) => {
  return{
    type: GET_BOARD_SUCCESS,
    result
  }
}
export const getBoardError = (error) => {
  return{
    type: GET_BOARD_FAILED,
    error,
  }
}

export const getBoard = () => {
  return function (dispatch){
    dispatch(getBoardRequest())
    axios
      .get('http://localhost:8080/board/project')
      .then(result => dispatch(getBoardSuccess(result.data)))
      .catch(error => dispatch(getBoardError(error)))
  }
}