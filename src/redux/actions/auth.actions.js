import axios from "axios";

export const AUTH_REQUEST = "AUTH_REQUEST";
export const AUTH_FAILED = "AUTH_FAILED";
export const REGISTER_SUCCESS = "REGISTER_SUCCESS";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGOUT = "LOGOUT";

export const authRequest = () => {
  return {
    type: AUTH_REQUEST,
  };
};

export const registerSuccess = (data) => {
  return {
    type: REGISTER_SUCCESS,
    payload: data,
  };
};

export const loginSuccess = (data) => {
  console.log('login data sukses action',data);
  return {
    type: LOGIN_SUCCESS,
    payload: data,
  };
};

export const authFailed = (err) => {
  return {
    type: AUTH_FAILED,
    err,
  };
};

export const logoutAction = () => {
  return {
    type: LOGOUT,
  };
};

export const registerAction =
  (data, history, ) => (dispatch) => {
    console.log('ini data di action', data);
    console.log('msk ke auth sblm axios');
    // event.preventDefault();
    dispatch(authRequest());

    return axios
      .post("http://localhost:8080/api/auth/signup", data)
      .then((result) => {
        // dispatch(registerSuccess(result.data));
        dispatch(registerSuccess(console.log(result.data)));
        // setRegister({
        //   ...data,
        //   basic_name: "",
        //   basic_username: "",
        //   basic_email: "",
        //   // basic_role: [""],
        //   basic_password: "",
        // })
        history.push("/login");
      })
      .catch((err) => dispatch(authFailed(err)));
  };

export const loginAction = (data, history, setLogin, setFail) => (dispatch) => {
  // event.preventDefault();
  console.log('ini data di action login', data);
  console.log('msk ke auth login sblm axios');
  dispatch(authRequest());

  return axios
    .post("http://localhost:8080/api/auth/signin", data)
    .then(result => {
      console.log(result);
      if (result.data.accessToken !== undefined) {
        localStorage.accessToken = result.data.accessToken
        localStorage.payload = JSON.stringify(result.data.data);
        dispatch(loginSuccess(result.data.accessToken))

        history.push('/');
      } else {
        setLogin({
          ...data,
          password: ""
        })
        setFail({
          result: false
        })
        dispatch(authFailed("invalid"));
      }

    })
    .catch(err => dispatch(authFailed(err)))
};
